FROM node:7-alpine

ENV NPM_CONFIG_LOGLEVEL warn
ENV NODE_ENV production
ENV HTTP_PORT 8080
WORKDIR /app
RUN chown -R node:node /app

USER node
COPY package.json /app/
RUN npm install --production --ignore-scripts
COPY . /app

CMD [ "npm", "start" ]

