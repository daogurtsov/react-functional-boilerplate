import path from 'path';
import express from 'express';
import compression from 'compression';

import { getConfig } from './config';
const config = getConfig();
const router = express.Router();

router.use((req, res, next) => {
    if (!req.redirectedToIndex) {
        console.log(`<<< INCOMING: `, req.method, req.originalUrl);
    }
    return next();
});

router.use(compression);
router.use(express.static(path.resolve('build/client'), { fallthrough: true }));

router.use('*', (req, res, next) => {
    if (!req.headersSent) {
        console.log('Redirecting to index.html');
        const newReq = Object.assign({}, req, { redirectedToIndex: true, url: '/index.html' });
        return router.handle(newReq, res);
    }
    return next();
});

express()
    .disable('x-powered-by')
    .use(router)
    .listen(config.http.port);
