import { config as configEnv } from 'dotenv';
import { has, defaults } from 'lodash';

configEnv();

const getLoggingConfig = (env, name) => {
    const level = env.LOGGING_LEVEL || 'debug';
    const stream = env.LOGGING_STREAM || 'stdout';
    return { name, [stream]: { level } };
};

const getHttpConfig = env => defaults(
    {
        protocol: env.HTTP_PROTOCOL,
        host: env.HTTP_HOST,
        port: env.HTTP_PORT ? parseInt(env.HTTP_PORT) : undefined
    },
    {
        protocol: 'http',
        host: 'localhost',
        port: 8080
    }
);

const getApiConfig = env => defaults(
    {
        baseUrl: env.API_BASE_URL
    },
    {
        baseUrl: 'http://localhost:3000'
    }
);

function getConfig(overrides = {}) {
    // eslint-disable-next-line no-process-env
    const env = { ...process.env, ...overrides };
    const name = has(env, 'NAME') ? env.NAME : 'Dashboard';
    const isProduction = has(env, 'NODE_ENV') && env.NODE_ENV === 'production';
    const isTesting = has(env, 'NODE_ENV') && env.NODE_ENV === 'testing';

    const logging = getLoggingConfig(env);
    const http = getHttpConfig(env);
    const api = getApiConfig(env);

    return {
        name,
        isProduction,
        isTesting,
        logging,
        http,
        api
    };
}

module.exports = { getConfig };
