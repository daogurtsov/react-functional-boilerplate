const express = require('express');
const cors = require('cors');
require('dotenv').config();
// eslint-disable-next-line no-process-env
const env = { ...process.env };

const createDummyRouter = () => {
    const router = express.Router();

    router.use((req, res, next) => {
        if (!req.redirectedToIndex) {
            console.log(`<<< INCOMING TO DUMMY: `, req.method, req.originalUrl);
        }
        return next();
    });

    router
        .options('*', cors({ origin: true }))
        .use(cors({ origin: true }))
        .use('/login', (req, res, next) => {
            res.json({
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJ1c2VybmFtZSI6InRlc3RAdGVzdC5jb20iLCJuYW1lIjoidGVzdCIsImxhc3RuYW1lIjoidGVzdDIifSwiaWF0IjoxNTEzNTAyMjkwLCJleHAiOjE1NDUwMzgyOTB9.ThtyzmxLsT0g-B5t-p6PIpO9ZqvjHhrTiRpAOzkJzq4'
            });
            return next();
        });

    return router;
};


const app = express();

app.use(createDummyRouter());
app.listen(env.DUMMY_HTTP_PORT || 2019);
