const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackConfig = require('../client/webpack.config');

require('dotenv').config();
// eslint-disable-next-line no-process-env
const env = { ...process.env };

const createWebpackRouter = () => {
    const router = express.Router();
    const compiler = webpack(webpackConfig);

    router.use((req, res, next) => {
        if (!req.redirectedToIndex) {
            console.log(`<<< INCOMING: `, req.method, req.originalUrl);
        }
        return next();
    });

    router.use(webpackDevMiddleware(compiler, {
        publicPath: '/',
        serverSideRender: false,
        stats: {
            colors: true,
            errors: true,
            cached: false,
            children: true,
            chunks: false
        }
    }));

    router
        .use('*', (req, res, next) => {
            if (!res.headersSent) {
                console.log('Redirecting to index.html');
                const newReq = Object.assign({}, req, { redirectedToIndex: true, url: '/index.html' });
                return router.handle(newReq, res);
            }
            return next();
        });

    return router;
};


const app = express();

app.use(createWebpackRouter());
app.listen(env.HTTP_PORT || 8080);
