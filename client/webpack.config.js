const { existsSync } = require('fs');
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const CommonsChunkPlugin = require('webpack').optimize.CommonsChunkPlugin;
const UglifyJsPlugin = require('webpack').optimize.UglifyJsPlugin;
require('dotenv').config();
// eslint-disable-next-line no-process-env
const env = { ...process.env };

const isProduction = env.NODE_ENV && env.NODE_ENV === 'production';

const logoPath = existsSync(resolve(`${__dirname}`, 'images')) ?
    resolve(`${__dirname}`, 'images', 'logo.png') :
    resolve(`${__dirname}`, '../client', 'images', 'logo.png')
;

const devPlugins = [
    new CommonsChunkPlugin({
        name: 'vendor',
        minChunks: Infinity
    }),
    new HtmlWebpackPlugin({
        template: './html/index.html'
    }),
    new FaviconsWebpackPlugin({
        logo: logoPath,
        title: 'Boilerplate',
        emitStats: false,
        icons: {
            favicons: true,
            android: false,
            appleIcon: false,
            appleStartup: false,
            coast: false,
            opengraph: false,
            twitter: false,
            yandex: false,
            windows: false
        }
    })
];

const plugins = isProduction ?
    [
        ...devPlugins,
        new UglifyJsPlugin({
            compress: { warnings: false },
            sourceMap: true
        })
    ] : devPlugins;


module.exports = {
    target: 'web',
    context: __dirname,
    devtool: 'inline-source-map',
    entry: {
        'app': './js/index.js',
        'vendor': [
            'styled-components',
            'isomorphic-fetch',
            'lodash',
            'redux',
            'react',
            'react-dom',
            'react-redux',
            'redux-first-router'
        ]
    },
    output: {
        filename: '[name].[chunkhash].js',
        path: resolve(`${__dirname}`, `../build/client`),
        publicPath: '/'
    },
    devServer: {
        inline: false,
        publicPath: '/',
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                include: [resolve(__dirname)],
                loader: 'babel-loader'
            },
            {
                test: /\.css?$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
                loaders: 'url-loader',
                options: {
                    limit: '1000',
                    publicPath: '/'
                }
            }
        ]
    },
    plugins
};
