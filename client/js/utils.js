// @flow

import { omitBy, isEmpty, partial as lodashPartial, keys, difference } from 'lodash';
import moment from 'moment';
import type { ActionError } from './shared/types/index';

export function removeEmpty(obj: Object): Object {
    return omitBy(obj, isEmpty);
}

export function createAction(type: string, payload: ?any): Object {
    return removeEmpty({ type, payload });
}

export function createErrorAction(type: string, error: Error): ActionError {
    return { type, error };
}

export function getImportedContainer(importPromise: Promise<any>, callback: Function): void {
    importPromise
        .then(container => callback(null, container.default))
        .catch(err => callback(err));
}

export function isDateToday(dateOrStr: Date|string): boolean {
    return moment(dateOrStr).isSame(new Date(), 'day');
}

export function isPositiveNumber(number: number): boolean {
    return Number.isInteger(number) && number > 0;
}

/* eslint-disable */
declare function PartialT<A1, A2, R>(((A1, A2) => R), A1) : (A2) => R;
declare function PartialT<A1, A2, A3, R>(((A1, A2, A3) => R), A1): (A2, A3) => R;
declare function PartialT<A1, A2, A3, R>(((A1, A2, A3) => R), A1, A2): (A3) => R;
declare function PartialT<A1, A2, A3, A4, R>(((A1, A2, A3, A4) => R), A1): (A2, A3, A4) => R;
declare function PartialT<A1, A2, A3, A4, R>(((A1, A2, A3, A4) => R), A1, A2): (A3, A4) => R;
declare function PartialT<A1, A2, A3, A4, R>(((A1, A2, A3, A4) => R), A1, A2, A3): (A4) => R;
declare function PartialT<A1, A2, A3, A4, A5, R>(((A1, A2, A3, A4, A5) => R), A1): (A2, A3, A4, A5) => R;
declare function PartialT<A1, A2, A3, A4, A5, R>(((A1, A2, A3, A4, A5) => R), A1, A2): (A3, A4, A5) => R;
declare function PartialT<A1, A2, A3, A4, A5, R>(((A1, A2, A3, A4, A5) => R), A1, A2, A3): (A4, A5) => R;
declare function PartialT<A1, A2, A3, A4, A5, R>(((A1, A2, A3, A4, A5) => R), A1, A2, A3, A4): (A5) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, R>(((A1, A2, A3, A4, A5, A6) => R), A1): (A2, A3, A4, A5, A6) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, R>(((A1, A2, A3, A4, A5, A6) => R), A1, A2): (A3, A4, A5, A6) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, R>(((A1, A2, A3, A4, A5, A6) => R), A1, A2, A3): (A4, A5, A6) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, R>(((A1, A2, A3, A4, A5, A6) => R), A1, A2, A3, A4): (A5, A6) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, R>(((A1, A2, A3, A4, A5, A6) => R), A1, A2, A3, A4, A5): (A6) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, R>(((A1, A2, A3, A4, A5, A6, A7) => R), A1): (A2, A3, A4, A5, A6, A7) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, R>(((A1, A2, A3, A4, A5, A6, A7) => R), A1, A2): (A3, A4, A5, A6, A7) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, R>(((A1, A2, A3, A4, A5, A6, A7) => R), A1, A2, A3): (A4, A5, A6, A7) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, R>(((A1, A2, A3, A4, A5, A6, A7) => R), A1, A2, A3, A4): (A5, A6, A7) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, R>(((A1, A2, A3, A4, A5, A6, A7) => R), A1, A2, A3, A4, A5): (A6, A7) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, R>(((A1, A2, A3, A4, A5, A6, A7) => R), A1, A2, A3, A4, A5, A6): (A7) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, A8, R>(((A1, A2, A3, A4, A5, A6, A7, A8) => R), A1): (A2, A3, A4, A5, A6, A7, A8) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, A8, R>(((A1, A2, A3, A4, A5, A6, A7, A8) => R), A1, A2): (A3, A4, A5, A6, A7, A8) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, A8, R>(((A1, A2, A3, A4, A5, A6, A7, A8) => R), A1, A2, A3): (A4, A5, A6, A7, A8) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, A8, R>(((A1, A2, A3, A4, A5, A6, A7, A8) => R), A1, A2, A3, A4): (A5, A6, A7, A8) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, A8, R>(((A1, A2, A3, A4, A5, A6, A7, A8) => R), A1, A2, A3, A4, A5): (A6, A7, A8) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, A8, R>(((A1, A2, A3, A4, A5, A6, A7, A8) => R), A1, A2, A3, A4, A5, A6): (A7, A8) => R;
declare function PartialT<A1, A2, A3, A4, A5, A6, A7, A8, R>(((A1, A2, A3, A4, A5, A6, A7, A8) => R), A1, A2, A3, A4, A5, A6, A7): (A8) => R;
/* eslint-enable */

export const partial: typeof PartialT = lodashPartial;

export function validate(context: string, path: string, validator: Function, value: *): boolean {
    const isValid = validator(value);
    if (isValid === false) {
        console.error(`Invalid value:`, { context, path, value, validator });
    }
    return isValid;
}

export function hasPaths(context: string, paths: Array<string>, source: Object): boolean {
    const sourcePaths = keys(source);
    const missingPaths = difference(paths, sourcePaths);
    if (missingPaths.length === 0) {
        return true;
    }
    console.error(`Paths are missing:`, { context, missingPaths, source });
    return false;
}
