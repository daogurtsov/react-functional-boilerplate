// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { configureStore } from './store';
import { Provider } from 'react-redux';
import AppContainer from './containers/app';
import '../css/global.css';

ReactDOM.render(
    <Provider store={configureStore()}>
        <AppContainer />
    </Provider>,
    document.getElementById('root')
);