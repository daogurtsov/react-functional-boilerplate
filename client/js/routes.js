import { performSearchArtistsAndEvents, setActiveEvent } from './containers/search_page/actions';
import { setSearchQuery } from './containers/search_page/actions';

export default {
    HOME_PAGE: {
        path: '/',
        thunk: dispatch => {
            const action = { type: 'SEARCH_PAGE' };
            dispatch(action);
        }
    },
    SEARCH_PAGE: '/search',
    SEARCH_PAGE_QUERY: {
        path: '/search/artist/:artistname',
        thunk: async(dispatch, getState) => {
            const { artistname } = getState().location.payload;
            const ephemAction = setSearchQuery(artistname);
            dispatch(ephemAction);
            performSearchArtistsAndEvents()({ dispatch, getState });
        }
    },
    EVENT_PAGE: {
        path: '/event/:id',
        thunk: (dispatch, getState) => {
            const { id } = getState().location.payload;
            const action = setActiveEvent(id)({ dispatch, getState });
            dispatch(action);
        }
    }
};