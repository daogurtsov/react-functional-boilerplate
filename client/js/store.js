// @flow
import { connectRoutes } from 'redux-first-router';
import persistState from 'redux-localstorage';
import createHistory from 'history/createBrowserHistory';
import routes from './routes';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { isFunction, has, isNil, isError, keys } from 'lodash';

import * as reducers from './reducers';
import type { Store } from './shared/types/index';

const history = createHistory();

const {
    reducer: routesReducer,
    middleware: routesMiddleware,
    enhancer: routesEnhancer
} = connectRoutes(history, routes);

const isPromise = val => (val && val.then && isFunction(val.then));

const createPersistStateEnhancer = () => {
    const merge = (initialState, persistedState) => ({ ...initialState, ...persistedState });
    return persistState(keys(reducers), { merge });
};

const middleware = store => next => dispatchable => {
    if (isNil(dispatchable)) {
        throw new Error(`Dispatchable must not be falsey (${dispatchable})`);
    }
    const action = isFunction(dispatchable) ? dispatchable(store) : dispatchable;
    console.info('Store.middleware', action);

    const promiseHandler = res => {
        if (isError(res)) {
            throw res;
        }
        if (!has(res, 'type')) {
            throw new Error(`Invalid action returned from promise: (${res})`);
        }
        next(res);
    };

    if (isPromise(action)) {
        return action.then(promiseHandler).catch(promiseHandler);
    }

    return next(action);
};

export function configureStore(initialState: Object = {}): Store {
    const reducer = combineReducers({ ...reducers, location: routesReducer });
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const enhancer = composeEnhancers(
        routesEnhancer,
        applyMiddleware(middleware, routesMiddleware),
        createPersistStateEnhancer()
    );
    const store = createStore(reducer, initialState, enhancer);

    return store;
}
