// flow
import styled from 'styled-components';

export const Input = styled.input`
    width: 100%;
    margin: 10px 0px;
    border-radius: 5px;
`;

export const SubmitInput = styled(Input)`
    background-color: ${props => props.disabled ? '#ccc' : props.theme.buttonBackgroundColor};
    color: ${props => props.theme.buttonColor || 'initial'};
    ${props => props.disabled ? '' : 'cursor: pointer;'};
`;

export const Error = styled.div`
    color: rgb(201,58,58);
`;