// @flow

import React from 'react';
import { join } from 'lodash';
import type { IconSize, IconFlip } from '../../types';

type Props = {
    name: string,
    style?: Object,
    className?: string,
    isSpin?: boolean,
    flip?: IconFlip,
    size?: IconSize,
    onClick?: Function
};

export default function Icon(props: Props) {
    const { name, isSpin, style = {}, className = '', flip = '', size = '', onClick } = props;
    const classNames = join(
        [
            `fa`,
            `fa-${name}`,
            `fa-${size}`,
            `${className}`
        ]
        , ' ');

    const classFlip = flip ? `fa-flip-${flip}` : '';
    const classSpin = isSpin ? 'fa-spin' : '';

    return <i className={`${classNames} ${classFlip} ${classSpin}`} style={style} onClick={onClick} />;
}
