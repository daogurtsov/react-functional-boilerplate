// @flow

import React from 'react';
import moment from 'moment';

import type { DateFormat } from '../../types';

type Props = {
	format: DateFormat,
	date: Date | number,
	className?: string
};

export default function DateTime(props: Props) {
    const { format, date, className } = props;
    const time = moment(date);

    return (
        <time className={className}>{time.format(format)}</time>
    );
}
