// @flow

import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import Icon from './icon';

type Props = {
	showSpinner: boolean;
	children?: any;
};

const styles = StyleSheet.create({
    loadingMessage: {
        width: '100%',
        height: '100%',
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: '28px',
        color: '#999',
        backgroundColor: '#FFF'
    },
    message: {
        marginBottom: '20px'
    }
});

export default function LoadingMessage(props: Props) {
    const { showSpinner, children } = props;

    return (
        <div className={css(styles.loadingMessage)}>
            <div className={css(styles.message)}>
                {children}
            </div>
            {showSpinner &&
				<Icon name="circle-o-notch" className="fa-fw" isSpin={showSpinner} />
            }
        </div>
    );
}
