// @flow

import React from 'react';
import styled from 'styled-components';

const NotFoundWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

type Props = {
    getString: Function;
};

export default function NotFound({ getString }: Props) {
    return (
        <NotFoundWrapper>{getString('NOT_FOUND')}</NotFoundWrapper>
    );
}