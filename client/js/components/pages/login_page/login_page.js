// @flow

import React from 'react';
import styled from 'styled-components';

import { Input, SubmitInput, Error } from '../../shared/styled';

import type { ChangeAuthFunction, LoginFunction } from '../../../shared/actions/session';
import type { LoginContainerProps } from '../../../containers/login_page/login_page';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    flex: 1;
    user-select: none;
    align-items: center;
    justify-content: center;
`;

const Form = styled.form`
    width: 300px;
    padding: 20px;
    border: 1px solid #ccc;
    background-color: #fff;
    border-radius: 5px;
`;

const Username = props => (<Input {...props} />);
const Password = props => (<Input {...props} />);

const Submit = SubmitInput.extend`
    margin-top: 20px;
`;

type Props = {
    ...LoginContainerProps,
    getString: Function,
    login: LoginFunction,
    changeAuth: ChangeAuthFunction
};

export default function LoginPage({ login, changeAuth, username, password, error, getString }: Props) {
    const isDisabled = Boolean(username === '' || password === '');
    const attrs = {
        form: {
            onSubmit: evt => {
                login();
                evt.preventDefault();
            }
        },
        username: {
            type: 'email',
            placeholder: getString('login.username.placeholder'),
            value: username,
            onChange: evt => {
                changeAuth('username', evt.target.value);
            }
        },
        password: {
            type: 'password',
            placeholder: getString('login.password.placeholder'),
            onChange: evt => {
                changeAuth('password', evt.target.value);
            }
        },
        submit: {
            type: 'submit',
            disabled: isDisabled,
            value: getString('login.submit')
        }
    };
    return (
        <Wrapper>
            <Form {...attrs.form}>
                { (error && error.msg === 'Unauthorized') &&
                    <Error>{getString('login.unauthorized')}</Error>
                }
                <Username {...attrs.username} />
                <Password {...attrs.password} />
                <Submit {...attrs.submit} />
            </Form>
        </Wrapper>
    );
}
