// @flow

import React from 'react';
import { NavLink } from 'redux-first-router-link';
import styled from 'styled-components';
import Icon from '../../shared/icon';

import type { GetString } from '../../../shared/types/strings';
import type { MenuItem as MenuItemType } from '../../../shared/types/app';

type Props = {
	isMenuOpen: boolean,
	item: MenuItemType,
	getString: GetString
};

const StyledNavLink = styled(NavLink)`
    display: flex;
    flex-direction: row;
    align-items: baseline;
    margin-bottom: 15px;
    cursor: pointer;
    width: 100%;
    text-decoration: none;
    justify-content: center;
    color: ${props => props.theme.menuTextColor || '#fff'};
    &:hover {
		color: ${props => props.theme.linkColorOnHoverAndVisited || '#00a6ff'} 
	}
    &:visited {
		color: ${props => props.theme.linkColorOnHoverAndVisited || '#00a6ff'} 
	}

`;

const StyledIcon = styled(Icon)`
    margin-right: 10px;
    font-size: 14px;
    width: 14px;
    ${props => props.isMenuOpen === false && `margin: 0; font-size: 28px; width: auto;`}
`;
const Text = styled.span`
    letter-spacing: 1.5px;
    font-size: 12pt;
    font-weight: 500;
`;

const getAttrs = ({ item, isMenuOpen }) => ({
    item: {
        activeStyle: {
            color: '#FFF !important'
        },
        key: item.path,
        to: item.path,
        disabled: Boolean(item.disabled),
        onClick: item.onClick ? item.onClick : () => {}
    },
    icon: {
        name: item.icon,
        isMenuOpen
    }
});

export default function MenuItem({ isMenuOpen, item, getString }: Props) {
    const attrs = getAttrs({ item, isMenuOpen });

    return (
        <StyledNavLink {...attrs.item}>
            <StyledIcon {...attrs.icon} />
            {isMenuOpen && <Text>{getString(item.text)}</Text>}
        </StyledNavLink>
    );
}
