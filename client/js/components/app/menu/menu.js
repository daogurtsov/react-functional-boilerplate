// @flow

import React from 'react';
import styled from 'styled-components';
import { map } from 'lodash';

import MenuItem from './menu_item';
import logo from '../../../../images/logo.png';

import type { MenuItem as MenuItemType } from '../../../shared/types/app';
import type { GetString } from '../../../shared/types/strings';

type Props = {
	isMenuOpen: boolean,
	menuItems: Array<MenuItemType>,
	getString: GetString
};

const MenuWrapper = styled.div`
    width: ${props => props.isMenuOpen === false ? '70' : '140'}px;
    height: 100%;
    display: flex;
    flex-direction: column;
    transition: 0.2s;
    background-color: ${props => props.theme.menuBackgroundColor || '#004D76'};
`;

const LogoWrapper = styled.div`
        width: 100%;
        height: 60px;
        padding: 5px 0;
        background-image: url(${logo});
        background-repeat: no-repeat;
        background-size: contain;
        background-position: center;
        background-origin: content-box;
        ${props => props.isMenuOpen === false && `background-image: url(${logo}); padding: 10px;`}
`;

const ItemsWrapper = styled.div`
    margin-top: 40px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center
`;

export default function Menu({ menuItems, isMenuOpen, getString }: Props) {
    return (
        <MenuWrapper isMenuOpen={isMenuOpen}>
            <LogoWrapper isMenuOpen={isMenuOpen}></LogoWrapper>
            <ItemsWrapper>
                {map(menuItems, item =>	<MenuItem key={item.text} {...{ item, isMenuOpen, getString } } />)}
            </ItemsWrapper>
        </MenuWrapper>
    );
}
