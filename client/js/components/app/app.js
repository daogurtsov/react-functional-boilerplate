// @flow

import React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import LoginPageContainer from '../../containers/login_page';
import NotFoundPage from '../../components/pages/not_found';
import Header from './header';
import Menu from './menu';

import type { AppContainerProps } from '../../shared/types/app';

type Props = {
    ...AppContainerProps,
    toggleMenu: Function,
    toggleSearch: Function,
    logout: Function,
    changeLocation: Function
};

const AppWrapper = styled.div`
        display: flex;
        flex-direction: row;
        align-content: flex-start;
        width: 100vw;
        height: 100vh;
        overflow: hidden;
    `;
const PageWrapper = styled.div`
        flex: 1;
        height: 100%;
        display: flex;
        flex-direction: column;
        background-color: ${props => props.theme.pageBackgroundColor};
  `;

export default function App(props: Props) {
    const { settings, session, toggleMenu, toggleSearch, logout, getString, routeName, changeLocation, isMenuOpen } = props;
    const { theme, menuItems } = settings;
    const { user } = session;
    const isLoggedIn = Boolean(user);
    if (!isLoggedIn && routeName !== 'LOGIN_PAGE') {
        changeLocation('LOGIN_PAGE');
    }
    const attrs = {
        menu: {
            isMenuOpen,
            menuItems,
            getString
        },
        header: {
            user,
            toggleMenu,
            toggleSearch,
            logout,
            getString
        }
    };
    return (
        <ThemeProvider theme={theme.style}>
            <AppWrapper>
                {isLoggedIn && <Menu {...attrs.menu} />}
                <PageWrapper>
                    {routeName === 'NOT_FOUND' && <NotFoundPage getString={getString}/>}
                    {!isLoggedIn && routeName === 'LOGIN_PAGE' && <LoginPageContainer />}
                    {isLoggedIn && <Header {...attrs.header} />}
                </PageWrapper>
            </AppWrapper>
        </ThemeProvider>
    );
}
