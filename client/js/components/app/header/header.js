// @flow

import React from 'react';
import styled from 'styled-components';
import Icon from '../../shared/icon';
import UserBox from './user_box';

import type { GetString } from '../../../shared/types/strings';
import type { Theme } from '../../../shared/types/app';
import type { User } from '../../../shared/types/session';

type Props = {
	getString: GetString,
	user: User,
	theme: Theme,
	toggleMenu: Function,
	toggleSearch: Function,
	logout: Function
};

const HeaderWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    height: 60px;
    min-height: 60px;
    padding: 0 10px;
    box-sizing: border-box;
    user-select: none;
    color: ${props => props.theme.headerTextColor || '#fff'};
    background-color: ${props => props.theme.headerBackgroundColor || '#0089D2'};
`;

const StyledIcon = styled(Icon)`
    cursor: pointer;
    font-size: 22px;
    flex: 1;
    color: ${props => props.theme.headerTextColor || '#fff'}
`;

const UserBoxWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const Logout = styled.div`
    padding: 5px;
    height: 30px;
    margin-left: 10px;
    cursor: pointer;
`;

export default function Header({ user, toggleMenu, logout, getString, theme }: Props) {
    const attrs = {
        menuIcon: {
            name: 'bars',
            onClick: toggleMenu
        },
        logout: {
            onClick: logout
        }
    };

    return (
        <HeaderWrapper>
            <StyledIcon {...attrs.menuIcon} />
            <UserBoxWrapper>
                <UserBox user={user} theme={theme} />
                <Logout {...attrs.logout}>{getString('logout')}</Logout>
            </UserBoxWrapper>
        </HeaderWrapper>
    );
}
