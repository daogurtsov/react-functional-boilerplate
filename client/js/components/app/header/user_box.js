// @flow

import React from 'react';
import Img from '../../shared/img';
import styled from 'styled-components';
import type { User } from '../../../shared/types/session';
import type { Theme } from '../../../shared/types/app';

type Props = {
	theme: ?Theme,
	user: User
};

const UserBoxWrapper = styled.div`
        display: flex;
        flex-direction: row;
        align-items: center;
`;

const CredsWrapper = styled.div`
        letter-spacing: 2px;
        line-height: initial;
        text-transform: uppercase;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-right: 10px;
`;

const StyledImage = styled(Img)`
    border-radius: 50%;
    box-shadow: 0 0 6px 3px rgba(255,255,255,.2);
    height: 50px;
`;

export default function UserBox(props: Props) {
    const { user } = props;
    const { image, firstName, lastName } = user;

    return (
        <UserBoxWrapper>
            <CredsWrapper>
                {firstName} {lastName}
            </CredsWrapper>
            <StyledImage src={image} alt={'thumbnail'} />
        </UserBoxWrapper>
    );
}
