// @flow
import { partial } from '../../utils';
import { connect } from 'react-redux';
import { replace } from 'redux-first-router';
import { errorSelector } from '../../shared/selectors/session';
import LoginPage from '../../components/pages/login_page/index';
import { getStringByPath } from '../app/app';
import { login as loginAction, changeAuth as changeAuthAction } from '../../shared/actions/session';
import type { State } from '../../shared/types/index';
import type { Dispatch } from 'redux';
import type { MetaError } from '../../shared/types/session';

const mapDispatchToProps = (dispatch: Dispatch<*>): * => {
    const login = () => dispatch(loginAction()).then(() => {
        replace('/');
    });

    const changeAuth = (flag, value) => dispatch(changeAuthAction(flag, value));

    return { login, changeAuth };
};

export type LoginContainerProps = {
    username: ?string,
     password: ?string,
     error: ?MetaError,
     getString: (string) => string
};

const mapStateToProps = (state:State):LoginContainerProps => ({
    username: state.session.username,
    password: state.session.password,
    error: errorSelector(state),
    getString: partial(getStringByPath, state)
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);