// @flow

import type { State } from '../types/index';
import type { AppSettings, PageModes } from '../types/app';

export const appSettingsSelector = (state: State): AppSettings => state.app.settings;

export const pageModesSelector = (state: State): PageModes => state.app.modes;
