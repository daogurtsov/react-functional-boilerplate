// @flow
import type { ActionCreator } from '../../shared/types';
import type { ToggleMenuAction, ToggleLoadingAction } from '../../shared/types/app';
import { createAction } from '../../utils';

export function toggleMenu(): ActionCreator<ToggleMenuAction> {
    return store => createAction(
        'TOGGLE_MENU',
        { isMenuOpen: !store.getState().app.modes.isMenuOpen }
    );
}

export function toggleLoading(): ActionCreator<ToggleLoadingAction> {
    return store => createAction(
        'TOGGLE_LOADING',
        { isLoading: !store.getState().app.modes.isLoading }
    );
}
