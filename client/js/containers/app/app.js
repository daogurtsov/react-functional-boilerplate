// @flow

import { partial } from '../../utils';

import { connect } from 'react-redux';
import App from '../../components/app';
import { currentRouteName } from '../../shared/selectors/location';
import * as appActions from '../../shared/actions/app';
import * as sessionActions from '../../shared/actions/session';

import { sessionSelector } from '../../shared/selectors/session';
import { appSettingsSelector, pageModesSelector } from '../../shared/selectors/app';
import { stringSelector } from '../../shared/selectors/strings';

import type { State } from '../../shared/types';
import type { StringPath } from '../../shared/types/strings';

import type { AppContainerProps } from '../../shared/types/app';

export function getStringByPath(state: State, path: StringPath): string {
    return stringSelector(state, { path });
}

const mapStateToProps = (state: State): AppContainerProps => {
    const pageModes = pageModesSelector(state);
    return {
        settings: appSettingsSelector(state),
        isLoading: pageModes.isLoading,
        isMenuOpen: pageModes.isMenuOpen,
        session: sessionSelector(state),
        routeName: currentRouteName(state),
        getString: partial(getStringByPath, state)
    };
};

const actions = { ...appActions, ...sessionActions };

export default connect(mapStateToProps, actions)(App);