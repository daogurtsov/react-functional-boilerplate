// @flow

import type { Strings } from '../shared/types/strings';

const initialState: Strings = {
    strings: {
        'login.unauthorized': 'Invalid username or password',
        'login.username.placeholder': 'Username',
        'login.password.placeholder': 'Password',
        'login.submit': 'NEXT',
        'logout': 'logout',
        'menu.home': 'Home'
    }
};

export default function(state: Strings = initialState): Strings {
    if (!state) return initialState;
    return state;
}
