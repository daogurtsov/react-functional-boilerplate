// @flow
import type { Action } from '../shared/types';
import type { App, ToggleMenuAction, ToggleSearchAction, ToggleLoadingAction } from '../shared/types/app';

type SupportedActions = ToggleMenuAction | ToggleSearchAction | ToggleLoadingAction;

const initialState: App = {
    modes: {
        isPageLoading: false,
        isMenuOpen: true
    },
    settings: {
        menuItems: [
            {
                text: 'menu.home',
                icon: 'home',
                path: '/'
            }
        ],
        api: {
            baseUrl: 'http://localhost:2019'
        },
        theme: {
            name: 'default',
            style: {
                headerBackgroundColor: '#0089D2',
                headerTextColor: '#ffffff',
                menuBackgroundColor: '#004D76',
                menuTextColor: '#fff',
                pageBackgroundColor: '#e8e8ed',
                buttonBackgroundColor: '#4285f4',
                linkColorOnHoverAndVisited: '#00a6ff',
                buttonColor: '#fff'
            }
        }
    }
};

export function onMenuToggled(state: App, action: ToggleMenuAction): App {
    const { isMenuOpen } = action.payload;
    return { ...state, modes: { ...state.modes, isMenuOpen } };
}

export function onLoadingToggled(state: App, action: ToggleLoadingAction): App {
    return {
        ...state,
        modes: { ...state.modes, isLoading: action.payload.isLoading }
    };
}

export function onError(state: App, action: Action): App {
    console.error(`reducers.app.error:`, action);
    return state;
}

export default function(state: App = initialState, action: SupportedActions): App {
    if (!state) return initialState;

    if (action.error) {
        return onError(state, action);
    }

    switch (action.type) {
        case 'TOGGLE_LOADING': return onLoadingToggled(state, action);
        case 'TOGGLE_MENU': return onMenuToggled(state, action);
        default: return state;
    }
}