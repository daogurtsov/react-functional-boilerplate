// @flow

import type { Action } from '../shared/types';
import type { Session, LoginAction, LogoutAction, AuthAction } from '../shared/types/session';

type SupportedActions = LoginAction | LogoutAction | AuthAction;

const initialState: Session = {
    user: null,
    token: null,
    error: null,
    username: '',
    password: ''
};

export function onLogin(state: Session, action: LoginAction): Session {
    return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        username: '',
        password: '',
        error: null
    };
}

export function onLogout(state: Session): Session {
    return {
        ...state,
        user: null,
        token: null
    };
}

export function onChangedAuthUsername(state: Session, action: AuthAction): Session {
    return {
        ...state,
        username: action.payload.value
    };
}

export function onChangedAuthPassword(state: Session, action: AuthAction): Session {
    return {
        ...state,
        password: action.payload.value
    };
}

export function onError(state: Session, action: Action): Session {
    console.error('SessionReducer.onError', action);
    return {
        ...state,
        error: { type: action.type, msg: action.error.message }
    };
}

export default function(state: Session = initialState, action: SupportedActions): Session {
    if (!state) return initialState;

    if (action.error) {
        return onError(state, action);
    }

    switch (action.type) {
        case 'LOGIN': return onLogin(state, action);
        case 'LOGOUT': return onLogout(state);
        case 'CLEAR_SESSION': return onLogout(state);
        case 'CHANGE_AUTH_USERNAME': return onChangedAuthUsername(state, action);
        case 'CHANGE_AUTH_PASSWORD': return onChangedAuthPassword(state, action);
        default: return state;
    }
}
