// @flow

import { omit } from 'lodash';

import type { Action } from '../shared/types';
import type {
    Ephemeral,
    SetEphemeralValueAction,
    ClearEphemeralValueAction,
    ClearEphemeralNamespaceAction,
    ClearAllEphemeralAction
} from '../shared/types/ephemeral';

type SupportedActions =
	| SetEphemeralValueAction
	| ClearEphemeralValueAction
	| ClearEphemeralNamespaceAction
	| ClearAllEphemeralAction;

const initialState: Ephemeral = {};


export function onSetEphemeralValue(state: Ephemeral, action: SetEphemeralValueAction): Ephemeral {
    const { namespace, key, value } = action.payload;
    return {
        ...state,
        [namespace]: {
            ...state[namespace],
            [key]: value
        }
    };
}

export function onClearEphemeralValue(state: Ephemeral, action: ClearEphemeralValueAction): Ephemeral {
    const { namespace, key } = action.payload;
    return {
        ...state,
        [namespace]: omit(state[namespace], [key])
    };
}

export function onClearEphemeralNamespace(state: Ephemeral, action: ClearEphemeralNamespaceAction): Ephemeral {
    const { namespace } = action.payload;
    return omit(state, [namespace]);
}

// eslint-disable-next-line no-unused-vars
export function onClearAllEphemeral(state: Ephemeral, action: ClearAllEphemeralAction): Ephemeral {
    return {};
}

export function onError(state: Ephemeral, action: Action): Ephemeral {
    console.log(action);
    return state;
}

export default function(state: Ephemeral = initialState, action: SupportedActions) {
    if (!state) return initialState;

    if (action.error) {
        return onError(state, action);
    }

    switch (action.type) {
        case 'SET_EPHEMERAL_VALUE': return onSetEphemeralValue(state, action);
        case 'CLEAR_EPHEMERAL_VALUE': return onClearEphemeralValue(state, action);
        case 'CLEAR_EPHEMERAL_NAMESPACE': return onClearEphemeralNamespace(state, action);
        case 'CLEAR_ALL_EPHEMERAL': return onClearAllEphemeral(state, action);
        default: return state;
    }
}
