// @flow
import jwtDecode from 'jwt-decode';
import type { ActionCreator, AsyncActionCreator, ActionError } from '../types';
import type { LoginAction, LogoutAction, ClearSessionAction, AuthAction } from '../types/session';

import { isEmpty } from 'lodash';
import sha256 from 'sha256';
import { createAction, createErrorAction } from '../../utils';
import { makeRequest } from '../../api';
import { loginStatusSelector } from '../selectors/session';

type ChangeAuth = ActionCreator<AuthAction | ActionError>;
export type ChangeAuthFunction = (flag: 'password' | 'username', value: string) => ChangeAuth;

export function changeAuth(flag: 'password' | 'username', value: string): ChangeAuth {
    if (flag === 'password') {
        return createAction('CHANGE_AUTH_PASSWORD', { value });
    }
    return createAction('CHANGE_AUTH_USERNAME', { value });
}

type Login = AsyncActionCreator<LoginAction | ActionError>;
export type LoginFunction = () => Login;
export function login(): Login {
    console.info('SessionActions.login');

    return async store => {
        const state = store.getState();
        const { username, password } = state.session;
        if (isEmpty(username) || isEmpty(password)) {
            const err = new Error('Empty username or password');
            return Promise.reject(err);
        }
        const { isLoggedIn } = loginStatusSelector(state);

        if (isLoggedIn) {
            const err = new Error('Cannot login: a user is already logged in');
            return createErrorAction('LOGIN', err);
        }

        try {
            const res = await makeRequest(store, {
                method: 'POST',
                path: 'login',
                body: { email: username, password: sha256(password) },
                authRequired: false
            });
            const { token } = res;
            const dToken = jwtDecode(token);
            const { user } = dToken;
            return createAction('LOGIN', { token, user });
        }
        catch (err) {
            return createErrorAction('LOGIN', err);
        }
    };
}

type Logout = AsyncActionCreator<LogoutAction | ActionError>;
export function logout(): Logout {
    console.info('SessionActions.logout');

    return async store => {
        const { isLoggedIn } = loginStatusSelector(store.getState());

        if (!isLoggedIn) {
            const err = new Error('Cannot logout: a user is not logged in');
            return createErrorAction('LOGOUT', err);
        }

        try {
            await makeRequest(store, {
                method: 'POST',
                path: 'logout',
                body: { logout: 'logout' },
                projectRequired: false
            });
            return createAction('LOGOUT');
        }
        catch (err) {
            return createErrorAction('LOGOUT', err);
        }
    };
}

type ClearSession = ActionCreator<ClearSessionAction>;
export function clearSession(): ClearSession {
    return createAction('CLEAR_SESSION');
}

export function changeLocation(routeName: string): ActionCreator<*> {
    return () => createAction(routeName);
}
