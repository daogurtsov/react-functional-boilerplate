// @flow

import type { ActionCreator } from '../types/index';

import type {
    SetEphemeralValueAction,
    ClearEphemeralValueAction,
    ClearEphemeralNamespaceAction,
    ClearAllEphemeralAction
} from '../types/ephemeral';

import { createAction } from '../../utils';

export function setEphemeralValue(namespace: string, key: string, value: *): ActionCreator<SetEphemeralValueAction> {
    return createAction('SET_EPHEMERAL_VALUE', { namespace, key, value });
}

export function clearEphemeralValue(namespace: string, key: string): ActionCreator<ClearEphemeralValueAction> {
    return createAction('CLEAR_EPHEMERAL_VALUE', { namespace, key });
}

export function clearEphemeralNamespace(namespace: string): ActionCreator<ClearEphemeralNamespaceAction> {
    return createAction('CLEAR_EPHEMERAL_NAMESPACE', { namespace });
}

export function clearAllEphemeral(): ActionCreator<ClearAllEphemeralAction> {
    return createAction('CLEAR_ALL_EPHEMERAL');
}
