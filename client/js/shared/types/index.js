// @flow

import type { MiddlewareAPI as ReduxMiddlewareAPI, Store as ReduxStore } from 'redux';
import type { App } from './app';
import type { Session } from '../selectors/session';
import type { Ephemeral } from './ephemeral';
import type { Strings } from '../selectors/strings';

export type State = {
	+app: App,
	+session: Session,
	+ephemeral: Ephemeral,
	+strings: Strings
};

export type ActionError = {|
	+type: string,
	+error: Error
|};

export type Action = ActionError | {
	+type: string;
};

export type MiddlewareAPI = ReduxMiddlewareAPI<*, *>;

export type Store = ReduxStore<*, *>;

export type ActionCreator<V: Action> =
 | V
 | (store: Store | MiddlewareAPI) => V;

export type AsyncActionCreator<V: Action> =
 | (store: Store | MiddlewareAPI) => Promise<V>
 | Promise<V>;


export type LinkPath =
 | string
 | {
	pathname: string,
	query: { [key: string]: string | number }
};

export type IconSize = 'lg' | '2x' | '3x' | '4x' | '5x';

export type IconFlip = 'horizontal' | 'vertical';

export type IconName = string;

export type DateFormat = string;

export type ToggleOptionsFunction = (areOptionsOpen: boolean) => {};

