// flow

export type Ephemeral = {
	[namespace: string]: {
		[key: string]: any
	}
};

export type SetEphemeralValueAction = {
	+type: 'SET_EPHEMERAL_VALUE',
	+payload: {
		+namespace: string,
		+key: string,
		+value: *
	}
};

export type ClearEphemeralValueAction = {
	+type: 'CLEAR_EPHEMERAL_VALUE',
	+payload: {
		+namespace: string,
		+key: string
	}
};

export type ClearEphemeralNamespaceAction = {
	+type: 'CLEAR_EPHEMERAL_NAMESPACE',
	+payload: {
		+namespace: string
	}
};

export type ClearAllEphemeralAction = {
	+type: 'CLEAR_ALL_EPHEMERAL'
};
