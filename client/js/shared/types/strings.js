// @flow

export type StringPath = string;

export type StringsMappings = {[key: StringPath]: string };

export type Strings = {
	strings: StringsMappings
};

export type GetString = (path: StringPath) => string;