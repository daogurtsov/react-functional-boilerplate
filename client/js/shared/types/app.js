// @flow
import { Session } from './session';
import type { GetString } from './strings';

export type Theme = {
    name: string,
    style: { [key: string]: * }
};

export type MenuItem = {
    +text: string,
    +icon: string,
    +path: string,
    +disabled?: boolean,
    +onClick?: Function
};

export type Menu = {
    +isOpen: boolean,
};

export type Search = {
    +isOpen: boolean
};

export type ToggleMenuAction = {
    +type: 'TOGGLE_MENU';
    +payload: {
        +isMenuOpen: boolean;
    }
};

export type ToggleLoadingAction = {
    +type: 'TOGGLE_LOADING',
    +payload: {
        +isLoading: boolean;
    }
};

export type PageModes = {
    +isLoading: boolean,
    +isMenuOpen: boolean
};

export type AppSettings = {|
    +theme: Theme,
    +menuItems: MenuItem[],
    +api: {
        baseUrl: string
    }
|};

export type App = {|
    +modes: PageModes,
    +settings: AppSettings
|};

export type AppContainerProps = {|
    +settings: AppSettings,
    +isLoading: boolean,
    +isMenuOpen: boolean,
    +session: Session,
    +routeName: string,
    +getString: GetString
|};
