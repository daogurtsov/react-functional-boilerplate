// flow
export type User = {
    id: number,
    projectId: number,
    customerId: number,
    firstName: string,
    lastName: string,
    image: string
};

export type MetaError = { type: string, msg: string };

export type Session = {
    user: ?User,
    token: ?string,
    username: ?string,
    password: ?string,
    error: ?MetaError
};

export type LoginStatus = {
    didChange: boolean,
    isLoggedIn: boolean
};

export type LoginAction = {
    type: 'LOGIN',
    payload: {
        user: User,
        token: string
    }
};

export type LogoutAction = {
    type: 'LOGOUT'
};

export type ClearSessionAction = {
    type: 'CLEAR_SESSION'
};

export type AuthAction = {
    type: 'CHANGE_AUTH_USERNAME' | 'CHANGE_AUTH_PASSWORD',
    payload: {
        +value: string
    }
};
