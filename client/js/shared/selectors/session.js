// @flow

import { get, isNil } from 'lodash';

import type { State } from '../types/index';
import type { User, LoginStatus, Session, MetaError } from '../types/session';

export function sessionSelector(state: State): Session {
    return state.session;
}

export function loggedInUserSelector(state: State): ?User {
    return get(state, 'session.user', null);
}

export function sessionTokenSelector(state: State): ?string {
    return get(state, 'session.token', null);
}

export function errorSelector(state: State): ?MetaError {
    return get(state, 'session.error', null);
}
// NOTE: !!! this is not a point of reference !!!
const loginStatusSelectorCreator = () => {
    /* eslint-disable fp/no-let, fp/no-mutation */
    let prevValue = null;

    return (state: State): LoginStatus => {
        const token = sessionTokenSelector(state);
        const didChange = prevValue !== !isNil(token);

        prevValue = !isNil(token);
        return { didChange, isLoggedIn: prevValue };
    };
    /* eslint-enable fp/no-let, fp/no-mutation */
};

export const loginStatusSelector = loginStatusSelectorCreator();
