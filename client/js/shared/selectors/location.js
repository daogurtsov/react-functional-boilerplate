// @flow
import type { State } from '../types/index';

export function currentPageName(state: State): string {
    return state.location.type;
}