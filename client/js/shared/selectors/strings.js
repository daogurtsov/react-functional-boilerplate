// @flow

import { createSelector } from 'reselect';

import type { State } from '../types/index';

import type { StringsMappings, StringPath } from '../types/strings';

function getStrings(state: State): StringsMappings {
    return state.strings.strings;
}

function getStringPath(_: State, props: { path: StringPath }): StringPath {
    return props.path;
}

export const stringSelector = createSelector(
    [getStrings, getStringPath],
    (defaultStrings: StringsMappings, path: StringPath): string => {
        const string = defaultStrings[path];
        if (string == null) {
            return path;
        }
        return string;
    }
);
