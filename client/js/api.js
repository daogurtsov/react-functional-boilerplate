// @flow

import 'isomorphic-fetch';
import moment from 'moment';
import { jwtDecode } from 'jwt-decode';
import { defaults, isNil, isEmpty, inRange, isString } from 'lodash';
import { format } from 'url';

import { removeEmpty } from './utils';
import { clearSession } from './shared/actions/session';

import type { Store, MiddlewareAPI } from './shared/types/index';

type RequestOptions = {
                    method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE',
                    baseUrl?: ?string,
                    path: string,
                    query?: { [key: string]: string|number|boolean },
                    body?: ?Object,
                    authRequired?: boolean
};

const requestOptionsDefaults = {
                            method: 'GET',
    baseUrl: null,
    path: '',
    query: {},
    body: null,
    authRequired: true
};

export async function makeRequest(store: Store | MiddlewareAPI, options: RequestOptions): Promise<any> {
const { session: { token }, app: { settings: { api: { baseUrl } } } } = store.getState();
                const { method, body, baseUrl: baseOverrideUrl, path, query, authRequired } = defaults(options, requestOptionsDefaults);
const bodyRequired = (method === 'POST' || method === 'PUT' || method === 'PATCH');

                        if (authRequired && isNil(token)) {
const err = new Error('Cannot make API request: there is no logged in user');
        return Promise.reject(err);
    }

if (bodyRequired && (isNil(body) || isEmpty(body))) {
        const err = new Error('Cannot make API request: body is empty or missing');
        return Promise.reject(err);
    }

    const pathname = `${baseOverrideUrl || baseUrl}/${path}`;

    const url = format({ pathname, query });

    const opts = removeEmpty({
        method,
        headers: removeEmpty({
            'Accepts': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': authRequired ? `Bearer ${token}` : null
        }),
        body: bodyRequired ? JSON.stringify(body) : null
    });
    const response = await fetch(url, opts);

    if (response.status === 403) {
        store.dispatch(clearSession());
    }

    if (!inRange(response.status, 200, 300)) {
        throw new Error(response.statusText);
    }
    return response.json();
}

export function isTokenExpired(token: string): boolean {
    if (isNil(token) || !isString(token) || isEmpty(token)) return true;
    const { exp } = jwtDecode(token);
    if (isNil(exp)) return true;
    return (moment.unix(exp).isSameOrBefore(Date.now()));
}
